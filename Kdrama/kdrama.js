var image= document.getElementsByClassName("Question1");
var image1= document.getElementsByClassName("Question2");
var image2= document.getElementsByClassName("Question3");
var image3= document.getElementsByClassName("Question4");
var image4= document.getElementsByClassName("Question5");
var image5= document.getElementsByClassName("Question6");
var image6= document.getElementsByClassName("Question7");
var image7= document.getElementsByClassName("Question8");
var image8= document.getElementsByClassName("Question9");
var image9= document.getElementsByClassName("Question10");

(function() {
    var questions = [{
      question: image ,
     
     
      choices: ['strong woman',' two worlds',' hwayugi', 'about time' ], 
      correctAnswer: 3
    }, {
      question: image1,
      choices: ['whats wrong with secretary kim', 'doctors',' weightlifting', 'Descendants of the sun (DOTS)'],
      correctAnswer: 4
    }, {
      question: image2,
      choices: ["romantic doctor", "doctors", "strong woman", "goblin"],
      correctAnswer: 1
    }, {
      question: image3,
      choices: ["doctors", "goblin", "strong woman", "hwayugi"],
      correctAnswer: 1
    }, {
      question: image4,
      choices: ["two worlds", "hyawugi", "im not a robot", "about time"],
      correctAnswer: 2
    }, {
      question: image5,
      choices: ["two worlds", "legend of the blue sea", "hwayugi"," strong woman"],
      correctAnswer: 1
    }, {
      question: image6,
      choices: ["descendants of the sun", "doctors", "romantic doctor", "weightlifting"],
      correctAnswer: 2
    }, {
      question: image7,
      choices: ["legend of the blue sea", "strong woman", "doctors", "weightlifting"],
      correctAnswer: 1
    }, {
      question: image8,
      choices: ["two worlds", "doctors", "strong woman", "romantic doctor"],
      correctAnswer: 1
    }, {
      question: image9,
      choices: ["strong woman", "about time", "two worlds", "weightlifting"],
      correctAnswer: 3
    }];
    
    var questionCounter = 0; //Tracks question number
    var selections = []; //Array containing user choices
    var quiz = $('#quiz'); //Quiz div object
    
    // Display initial question
    displayNext();
    
    // Click handler for the 'next' button
    $('#next').on('click', function (e) {
      e.preventDefault();
      
      // Suspend click listener during fade animation
      if(quiz.is(':animated')) {        
        return false;
      }
      choose();
      
      // If no user selection, progress is stopped
      if (isNaN(selections[questionCounter])) {
        alert('Please make a selection!');
      } else {
        questionCounter++;
        displayNext();
      }
    });
    
    // Click handler for the 'prev' button
    $('#prev').on('click', function (e) {
      e.preventDefault();
      
      if(quiz.is(':animated')) {
        return false;
      }
      choose();
      questionCounter--;
      displayNext();
    });
    
   
    
    // Animates buttons on hover
    $('.button').on('mouseenter', function () {
      $(this).addClass('active');
    });
    $('.button').on('mouseleave', function () {
      $(this).removeClass('active');
    });
    
    // Creates and returns the div that contains the questions and 
    // the answer selections
    function createQuestionElement(index) {
      var qElement = $('<div>', {
        id: 'question'
      });
      
      var header = $('<h2>Question ' + (index + 1) + ': Title of this Drama?</h2>');
      qElement.append(header);
      
      var question = $('<p>').append(questions[index].question);
      qElement.append(question);
      
      var radioButtons = createRadios(index);
      qElement.append(radioButtons);
      
      return qElement;
    }
    
    // Creates a list of the answer choices as radio inputs
    function createRadios(index) {
      var radioList = $('<ul>');
      var item;
      var input = '';
      for (var i = 0; i < questions[index].choices.length; i++) {
        item = $('<li>');
        input = '<input type="radio" name="answer" value=' + i + ' />';
        input += questions[index].choices[i];
        item.append(input);
        radioList.append(item);
      }
      return radioList;
    }
    
    // Reads the user selection and pushes the value to an array
    function choose() {
      selections[questionCounter] = +$('input[name="answer"]:checked').val();
    }
    
    // Displays next requested element
    function displayNext() {
      quiz.fadeOut(function() {
        $('#question').remove();
        
        if(questionCounter < questions.length){
          var nextQuestion = createQuestionElement(questionCounter);
          quiz.append(nextQuestion).fadeIn();
          if (!(isNaN(selections[questionCounter]))) {
            $('input[value='+selections[questionCounter]+']').prop('checked', true);
          }
          
          // Controls display of 'prev' button
          if(questionCounter === 1){
            $('#prev').show();
          } else if(questionCounter === 0){
            
            $('#prev').hide();
            $('#next').show();
          }
        }else {
          var scoreElem = displayScore();
          quiz.append(scoreElem).fadeIn();
          $('#next').hide();
          $('#prev').hide();
          $('#start').show();
        }
      });
    }
    
    // Computes score and returns a paragraph element to be displayed
    function displayScore() {
      var score = $('<p>',{id: 'question'});
      
      var numCorrect = 0;
      for (var i = 0; i < selections.length; i++) {
        if (selections[i] === questions[i].correctAnswer) {
          numCorrect++;
        }
      }
      
      score.append('You got ' + numCorrect + ' questions out of ' +
                   questions.length + ' right!!! Press f5 to restart the game');
      return score;



    }
  })();